<!DOCTYPE html>
<html>
<head>
	<title>tentukan nilai</title>
</head>
<body>
	<?php
		function tentukan_nilai($number)
		{
		    if($number >= 85 && $number <= 100){
		    	echo "Nilai ". $number . " Sangat Baik <br>";
		    } else if ($number >= 70 && $number < 85){
		    	echo "Nilai ". $number . " Baik <br>";
		    } else if ($number >= 60 && $number < 70){
				echo "Nilai ". $number . " Cukup <br>";
			} else {
				echo "Nilai ". $number . " Kurang <br>";
			}

		    
		}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>

</body>
</html>